#!/usr/bin/perl -w
# ======================================================================
# PACKAGE BUNDLER ------------------------------------------------------
# This script bundles some packages into the archive to be installed
# automatically.  Packages are downloaded by pkg_add using the source
# configured by $PKG_PATH or /etc/pkg.conf.
# ======================================================================
# Copyright (C) 2007-2015, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------
use warnings;
use strict;
use autodie;

use Digest::MD5 qw(md5_hex);
use File::Spec::Functions qw(:ALL);
use File::Path qw(remove_tree mkpath);
use File::Find qw(find);
use File::Temp qw();
use POSIX;

select STDERR;
$| = 1;

# Sub to run a command, and die if the command fails.
#@[mysys
sub mysys {
	use strict;

	my %allow = (0 => 1);
	ref $_[0] eq 'ARRAY'
	  and %allow = map { int($_) => 1 } @{shift()};

	my $r = system(@_);

	$r == -1 and die("@_: failed: $!");
	($r & 127)
	  and die(  "@_: died, signal "
		  . ($r & 127)
		  . ", coredump "
		  . (($r & 128) ? "present" : "absent"));
	$r = $r >> 8;
	$allow{$r}
	  or die(
		"@_: exited with value $r; expected " . join(", ", sort(keys %allow)));
	1;
}

#@]

# Routines to fetch content from a remote URL.
#@[webfetch
sub webfetch {
	use strict;
	use LWP::UserAgent;

	my($url, $expect, $refer) = @_;

	my $ua = new LWP::UserAgent();
	$ua->agent($ENV{'http_useragent'} || '');
	$ENV{'http_proxy'}
	  and $ua->proxy(['http', 'https'], $ENV{'http_proxy'});
	$ua->protocols_allowed(['http', 'https']);
	$ua->cookie_jar({});

	my $req = HTTP::Request->new('GET' => $url);
	$refer and $req->header(Referer => $refer);

	my $resp = $ua->request($req);
	$resp->is_success() or die("$url: " . $resp->status_line());
	$expect
	  and($resp->content_type() =~ m#^\Q$expect\E(;.*)?$#
		or die("$url: expected $expect, received " . $resp->content_type()));

	$resp->content();
}

#@]
my $cachedir = catdir(getcwd(), "cache");
mkpath($cachedir);
-d $cachedir or die("failed to mkpath($cachedir) $!");

sub fetchweb($) {
	my $url = $_[0];

	my $key = md5_hex($url);
	my $cachefile = catfile($cachedir, $key);
	if(-s $cachefile) {
		my $fh;
		if(open($fh, "<", $cachefile)) {
			my $x = do { local $/; <$fh> };
			close($fh);
			print "cache hit: $key\n";
			return $x;
		}
	}

	print "fetch: $url\n";
	my $x = webfetch($url);

	mkpath("cache");
	my $fh;
	if(open($fh, ">", $cachefile)) {
		print $fh $x;
		close($fh);
	}
	return $x;
}

# Routine to fetch content from either a local file or URL.
sub fetch($) {
	my $url = $_[0];
	$url =~ m#://# and return fetchweb($url);
	print "read: $url\n";
	my $fh;
	open($fh, '<', $url) or die($!);
	my $r = do { local $/; <$fh> };
	close($fh);
	return $r;
}

# Automatic PKG_PATH selection logic.
my $PKG_PATH = $ENV{PKG_PATH};
my %avail    = ();

sub trygetpkgs() {

	# Get a list of all available packages.  Only PKG_PATH is supported, so
	# that this can be run from different versions of OpenBSD, or even non-
	# OpenBSD systems (that support the necessary commands).  HTTP and file
	# paths should be supported.
	if($PKG_PATH =~ m#^http://#) {
		my $html = eval { fetch($PKG_PATH); };
		$html or return;
		$PKG_PATH =~ s#/$##;
		while($html =~ m#href\s*=\s*['"]*([^'"]+\.tgz)#gi) {
			$avail{$1} = "$PKG_PATH/$1";
		}
	}
	else {
		for my $f (glob("$PKG_PATH/*.tgz")) {
			my $e = $f;
			$e =~ s#.*/##;
			$avail{$e} = $f;
		}
	}
}

# If the user has manually specified a mirror to use, then try that one first.
# Note that PKG_PATH may have different meanings on different host OS's, and we
# do NOT require running on OpenBSD to build this dist, so ignore paths that
# point somewhere without OpenBSD packages.
$PKG_PATH and trygetpkgs();

# If no packages were found at the manually-specified path, then try to
# automatically get one, using a mirror selected for us by the installer
# CGI at openbsd.org.
if(!%avail) {
	$ENV{PKG_REL}
	  or die("automatic \$PKG_PATH selection requires \$PKG_REL");
	my @mirrors = grep { m#\S# }
	  map { m#(\S+://\S+)# and $1 or '' }
	  split(m#\n#, fetchweb("http://www.openbsd.org/cgi-bin/ftplist.cgi"))
	  or die("unable to find any mirror URLs");
	while(!%avail and @mirrors) {
		$PKG_PATH = shift(@mirrors);
		$PKG_PATH =~ s#/(?:\d+\.\d|snapshots?)/.*##;
		$PKG_PATH = "$PKG_PATH/$ENV{PKG_REL}";
		trygetpkgs();
	}
}

%avail or die("unable to find any packages");
print "found " . scalar(keys %avail) . " packages\n";

# Combine package lists for each included module.
my %reffed = ();
do {
	open(my $ifh, "<", "pkglist") or die($!);
	my $list = do { local $/; <$ifh> };
	close($ifh);
	$reffed{$1} = 1 while($list =~ m#(\S+)#g);
};
my @needed = keys %reffed;

# Create the package output dir.
my $outdir = catdir(getcwd(), shift(@ARGV) || die());
-d $outdir or die("$outdir does not exist");

# Keep track of local packages.
opendir(my $dh, $outdir) or die($!);
my %localpkgs = map { $_ => 1 } readdir($dh);
closedir($dh);

# We should probably assume that we always need
# the quirks package, if available.
grep { m#^quirks-# } keys %avail
  and unshift @needed, "quirks--";

# Setup a temp dir.
my $tmpdir = File::Temp->newdir();
my $tmpd   = $tmpdir->dirname();
chdir($tmpd);
eval {
	my %selected = ();
	while(my $p = shift(@needed)) {
		$p =~ m#-[\d-]# or $p .= "--";
		$p =~ s#-0$#--#; # ezpkg hack
		my $rx = quotemeta($p);
		$rx =~ s#\\-\\-#-(\\d[^-]*)-?#;

		my @matches = grep { m#^$rx\.tgz$# } keys %avail;
		@matches or @matches = grep { m#^$rx\.tgz$# } keys %localpkgs;
		@matches or @matches = grep { m#^$rx# } keys %avail;
		scalar(@matches) or die("no match for ^$rx");
		scalar(@matches) > 1
		  and die("ambiguous match for ^$rx: " . join(", ", @matches));
		$p = $matches[0];

		$selected{$p} and next;
		$selected{$p} = 1;

		my $f = "$outdir/$p";
		my $u = $avail{$p};
		if($u and !(-s $f)) {
			$u =~ m#://# or $u = "file://$u";
			my $d = fetch($u);
			my $fh;
			open($fh, '>', $f) or die($!);
			print $fh $d;
			close($fh);
		}
		-s $f or die("failed to fetch $f");

		-f "$tmpd/+CONTENTS" and unlink("$tmpd/+CONTENTS");
		mysys("tar", "-xzf", $f, "+CONTENTS");

		open(my $fh, "<", "+CONTENTS") or die($!);
		while(<$fh>) {
			chomp;
			m#^\@depend\s.*:([^:]+)$# and push @needed, $1;
		}
		close($fh);
	}
};
chdir("/");
$@ and die($@);
