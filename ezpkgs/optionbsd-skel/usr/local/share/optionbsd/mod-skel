#!/usr/bin/perl -w
# ======================================================================
# Copyright (C) 2007-2015, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------
use strict;
use warnings;
use autodie;
use Digest::MD5 qw(md5_hex);
use Warr1024::OptionBSD::TweakLib;
use File::Copy qw(cp);

chdir("/usr/local/share/optionbsd/skel") or die($!);

# ......................................................................
# Setup

# Subroutine to get a "signature" of skel-files from a dir, so we can
# compare the installuser's home to /etc/skel.
sub skelsig {
	my($p, $ign) = @_;
	my %data;
	opendir(my $dh, $p) or die($!);
	while(my $e = readdir($dh)) {
		my $f = "$p/$e";
		$e =~ m#[^\.]# or next;
		if(!($e =~ m#^(?:dot)?\.#)) {
			$ign and next;
			die("non-dot-file: $f");
		}
		$e =~ s#^dot##;
		$data{$e} = (! -d $f) ? md5_hex(readfile($f)) : "";
	}
	closedir($dh);
	md5_hex(join(":", map { $_ => $data{$_} } sort keys %data));
}

# Make sure that the existing skel-files aren't already correct.
# We don't need to run anything further if they are.
my $oldsig = skelsig("/etc/skel");
$oldsig eq skelsig(".", 1) and exit(0);

# A user may have been created during the system install, in which case
# that user will have been created using the old skel-files.  If that's
# the case, and the user hasn't customized their files at all, then
# we'll clear out the old skel files and install the new ones.
my $home;
try(    sub {
		my $user = installuser();
		skelsig($user->{dir}) eq $oldsig
		  and $home = $user->{dir};
	},
	sub { undef($home) });

# ......................................................................
# Install system-wide config and scripts.

# Remove all the old skel-files.
moveaway("/etc/skel");
checkdir("/etc/skel");

# Install the new skel-files.
opendir(my $dh, ".") or die($!);
while(my $e = readdir($dh)) {
	$e =~ m#^dot\.# or next;
	inst(0644, 0, 0, $e, "/etc/skel/$e");
}
closedir($dh);

# ......................................................................
# Update root's configuration

# Install ONLY the kshrc file for root, since the profile script
# does some more complex setup that may not be appropriate for
# a primarily fail-safe account; root should only really be accessed
# via a non-login shell (i.e. su/doas) anyway.
if(!-e "/root/.skel") {
	touch(0600, "/root/.skel");
	inst(0644, 0, 0, "dot.kshrc", "/root/.kshrc");
	inst(0644, 0, 0, "dot.kshrc", "/root/.profile");
}

# ......................................................................
# Update install-time non-root user's configuration.

# Only update if we found a valid user, and the skelfiles
# have actually changed.
if($home and $oldsig ne skelsig("/etc/skel")) {

	# Clean out old skel-files.
	opendir($dh, $home) or die($!);
	while(my $e = readdir($dh)) {
		my $f = "$home/$e";
		-f $f or next;
		unlink($f);
	}
	closedir($dh);

	# Install new skel-files.
	opendir($dh, "/etc/skel") or die($!);
	while(my $e = readdir($dh)) {
		my $s = "/etc/skel/$e";
		$e =~ s#^dot##;
		cp $s, "$home/$e";
	}
	closedir($dh);
}
