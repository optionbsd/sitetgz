# ======================================================================
# SYSTEM-WIDE KSH PROFILE SCRIPT ---------------------------------------
# This script will be automatically called by the user's .profile script
# (from /etc/skel), and will perform some system-wide-default setup.
# ======================================================================
# Copyright (C) 2011, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------

umask 022
export HOME TERM
export PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin

# Load system-wide default shell settings, then user's custom settings.
if [ -r /etc/site.kshrc ]; then
	. /etc/site.kshrc
fi
if [ -r ~/.kshrc ]; then
	. ~/.kshrc
fi

# Automatically generate local SSH identity keys in background.
/usr/local/bin/ssh-keygen-auto >/dev/null 2>&1

# Automatically run tmux if it's considered "safe" on our terminal.
if /usr/local/bin/tmuxable >/dev/null 2>&1; then
	# Detect if the session already exists, and create and
	# configure it if it doesn't, otherwise attach to the
	# existing default one.
	if /usr/bin/tmux ls >/dev/null 2>&1; then
		exec /usr/bin/tmux attach
	else
		exec /usr/bin/tmux new -s "`whoami`@`hostname -s`"
	fi
fi
