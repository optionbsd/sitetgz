# ======================================================================
# SYSTEM-WIDE KSH RESOURCE SCRIPT --------------------------------------
# This script will be automatically called by the user's .kshrc script
# (from /etc/skel), and will perform some system-wide-default setup.
# ======================================================================
# Copyright (C) 2011, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------

# Basic default settings.
umask 022
export HOME TERM DISPLAY LINES COLUMNS
export PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin
export PATH=$PATH:/usr/X11R6/bin:/usr/games
export PATH=$PATH:/usr/local/bin:/usr/local/sbin
export ENV=$HOME/.kshrc
export HOST="`hostname -s`"
export PS1='${USER}@${HOST}:${PWD}> '
export PAGER=more
export TOP='-CHSs 1'
export LESS='-fr'
export CVSROOT=anoncvs@anoncvs1.ca.openbsd.org:/cvs

# If the m:tier stable signify key is installed locally, then add
# m:tier stable packages to PKG_PATH.
if [ -s /etc/installurl ]; then
	PKG_PATH="$(head -n 1 /etc/installurl)/$(uname -r)/packages/$(arch -s)"
	if [ -s "/etc/signify/mtier-`uname -r | tr -cd 0-9`-pkg.pub" ]; then
		export PKG_PATH="https://stable.mtier.org/updates/$(uname -r)/$(arch -s):$PKG_PATH"
	fi
	export PKG_PATH
fi

alias quit="exit"
alias logoff="logout"
alias ll="ls -alFho"
alias pl="ps -rax -o %cpu,time,%mem,stat,user,pid,command | sort -nk 1,3"
alias logs="tail -n 100 -f /var/log/syslog"
if [ "`whoami`" != "root" ]; then
	alias su="su -l"
fi

# Users, other than root, in group wheel should be able to sudo
# certain things.  This depends on the "doas" mod and "doasudo"
# package being installed to actually work, though.
if [ `id -u` != 0 ] && id -Gn | grep -q wheel; then
	alias mount='sudo /sbin/mount'
	alias umount='sudo /sbin/umount'
	alias pkg_add='sudo /usr/sbin/pkg_add'
	alias pkg_delete='sudo /usr/sbin/pkg_delete'
	alias syspatch='sudo /usr/sbin/syspatch'
	alias shutdown='sudo /sbin/shutdown'
fi

# Most users have no interest in coredumps.
ulimit -S -c 0
