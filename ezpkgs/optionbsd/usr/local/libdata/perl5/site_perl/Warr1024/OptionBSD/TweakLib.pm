# ======================================================================
# Copyright (C) 2007-2015, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------
use strict;
use warnings;
use autodie;

package Warr1024::OptionBSD::TweakLib;
our $VERSION = "1.0";

use Exporter ();
our @ISA    = "Exporter";
our @EXPORT = qw(
  try
  mysys
  checkdir
  backupsuffix
  moveaway
  inst
  touch
  readfile
  modifile
  modsysctl
  modsection
  installuser
);

use File::Path qw(mkpath);
use File::Spec::Functions qw(catfile);
use POSIX qw(strftime);

########################################################################

# Make sure standard system bin paths, as well as packages,
# are in PATH env for mysys use.
INIT {
	my $pathpref = join(
		":", qw(
		  /bin
		  /usr/bin
		  /sbin
		  /usr/sbin
		  /usr/X11R6/bin
		  /usr/local/bin
		  /usr/local/sbin));
	$ENV{PATH} =~ m#^\Q$pathpref\E#
	  or $ENV{PATH} = "$pathpref:$ENV{PATH}";
}

# Handle signals by dying, so we can trap them and perform cleanup
# as necessary afterwards.
INIT {
	map {
		$SIG{$_} = sub { die "SIG@_"; };
	} qw(TERM INT HUP ALRM PIPE);

	$SIG{__WARN__} = sub {
		my $x = "@_";
		chomp($x);
		$x =~ s# at \S+ line \d+.$##;
		print STDERR "$x$/";
	};
}

# Error handling helper.  Try to do something in an eval.  If it
# dies, run the catch handler.  Always run the finally handler before
# returning/dying.
sub try {
	my($try, $catch, $finally) = @_;
	my $r = eval { $try->() };
	if($catch and $@ and !($@ =~ m#^SIG#)) {
		my $err = $@;
		undef $@;
		$r = eval { $catch->($err) };
	}
	$finally and $finally->();
	$@ and die($@);
	$r;
}

# Snippet: run a command via shell, and handle return status
# automatically, dying on failure.
#@[mysys
sub mysys {
	use strict;

	my %allow = (0 => 1);
	ref $_[0] eq 'ARRAY'
	  and %allow = map { int($_) => 1 } @{shift()};

	my $r = system(@_);

	$r == -1 and die("@_: failed: $!");
	($r & 127)
	  and die(  "@_: died, signal "
		  . ($r & 127)
		  . ", coredump "
		  . (($r & 128) ? "present" : "absent"));
	$r = $r >> 8;
	$allow{$r}
	  or die(
		"@_: exited with value $r; expected " . join(", ", sort(keys %allow)));
	1;
}

#@]

# Make sure directories exist, create them if necessary, and die if
# we are unable to create them for any reason.
sub checkdir {
	foreach my $d (@_) {
		while(-l $d) { $d = readlink($d) // die("invalid symlink $d"); }
		-d $d and next;
		mkpath($d);
		-d $d or die("failed to mkpath $d: $!");
	}
	1;
}

# Manage backup suffix for displaced files.  Note that the value
# is defined by the topmost process that loads this library, and set
# in the $ENV automatically to be inherited by scripts executed by
# it, so suffixes will be consistent across multiple modules launched
# at the same time by one master process.
do {
	my $bkenv = "TweakLibBackupSuffix";

	sub backupsuffix() {
		$ENV{$bkenv}
		  ||= (".sitedist." . strftime("%Y%m%d%H%M%S", localtime(time())));
	}
	INIT { backupsuffix(); }
};

sub moveaway {
	foreach my $f (@_) {
		-e $f or next;
		my $b = $f . backupsuffix();
		rename($f, $b) or die($!);
	}
}

sub inst {
	my($m, $u, $g, $s, $d) = @_;
	$m = $m & 0777;

	$d =~ m#(.+)/# and checkdir($1);

	my @cmd = (
		qw(install -C -S -b -B),
		backupsuffix(), qw(-p -o),
		$u, "-g", $g, "-m", sprintf("%#o", $m),
		$s, $d);
	mysys(@cmd);

	return 1;
}

sub touch {
	my $mode = shift();
	foreach my $f (@_) {
		-e $f and next;
		my $fh;
		open($fh, ">>", $f) or die("$f: $!");
		close($fh);
		chmod($mode, $f) or die("$f: $!");
		chown(0, 0, $f) or die("$f: $!");
	}
}

sub readfile {
	my $f = shift();
	my $fh;
	open($fh, "<", $f) or die("$f: $!");
	my $r = do { local $/; <$fh>; };
	close($fh);
	return $r;
}

sub modifile {
	my($f, $e, $t) = @_;
	my($ifh, $ofh);

	open($ifh, "<", $f) or die("$f: $!");
	my @lines = <$ifh>;
	close($ifh);
	my $oldtxt = join("", @lines);

	if($t) {
		open($ifh, "<", $t) or die("$t: $!");
		@lines = <$ifh>;
		close($ifh);
	}

	chomp(@lines);
	@lines = @{$e->(@lines)};
	@lines = map { "$_$/" } @lines;

	my $newtxt = join("", @lines);
	$newtxt eq $oldtxt and return 1;

	my $g = "$f.sitenew";
	open($ofh, ">", $g) or die("$g: $!");
	print $ofh @lines;
	close($ofh);

	my($dev, $ino, $mode, $nlink, $uid, $gid) = stat($f);
	inst($mode, $uid, $gid, $g, $f) and unlink($g);
}

sub modsysctl {
	my $f   = shift();
	my %set = @_;
	modifile(
		$f,
		sub {
			my %vals;
			my @r = map { m|^\s*([^=]+)=(\S+)| and $vals{$1} = $2; $_ } @_;
			push @r,
			  map { "$_=$set{$_}" }
			  sort grep { !defined($vals{$_}) or $vals{$_} ne $set{$_} } keys %set;
			\@r;
		});
}

sub modsection {
	my $tagstart = shift();
	my $newblock = shift();
	my $tagtop   = "$tagstart begin";
	my $tagbot   = "$tagstart end";

	# Allow disabling this modification to manage a custom
	# site-specific version of it.
	grep { $_ eq "$tagstart skip" } @_ and return \@_;

	# Remove old versions of this code first, including any
	# leading whitespace just above the begin block.
	my(@r, $skip);
	for my $x (@_) {
		if($x eq $tagtop) {
			$skip = 1;
			while(@r and !($r[-1] =~ m#\S#)) {
				pop @r;
			}
		}
		$skip or push @r, $x;
		$x eq $tagbot and $skip = 0;
	}

	# Make sure there's a blank line at the end of the
	# file before our tweak section, for readability,
	# unless the file is otherwise totally blank.
	@r and($r[-1] eq '' or push @r, '');

	# Install the new tweak section.
	chomp($newblock);
	push @r, $tagtop, $newblock, $tagbot;

	\@r;
}

# Use a heuristic to detect new users added at install-time:
#   - Root's smtpd .forward will point to the new user.
#   - The user will have an "empty" home dir, with nothing but skel dotfiles.
sub installuser {
	my $u = readfile("/root/.forward");
	$u and $u =~ m#\S# or die("no root forward user");
	chomp($u);
	$u =~ m#[|:@/]# and die("invalid root forward user $u");

	my @names = qw(name pw uid gid quota comment gcos dir shell expire);
	my @stats = getpwnam($u) or die("invalid user passwd entry $u");
	my %user  = map { $names[$_] => $stats[$_] }
	  grep { $names[$_] } 0 .. (scalar(@stats) - 1);

	-d $user{dir} or die("invalid home dir $user{dir}");

	\%user;
}

########################################################################

1;
