# ======================================================================
# PF FIREWALL RULESET --------------------------------------------------
# This is a basic, reasonably secure ruleset for the pf packet filter.
# ======================================================================
# Copyright (C) 2011, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------


# ......................................................................
# MACROS

keeptcp  = "flags S/SAFR modulate state"
keepudp  = "keep state"
inetok   = "! <nowan>"
pubport  = "from any to any port"

# ......................................................................
# TABLES

table <nowan> const {127/8, 224/8, 240/8, 0/8, 255/8}

# ......................................................................
# OPTIONS

set limit states 65536
set timeout { adaptive.start 32768, adaptive.end 65535 }
set optimization conservative
set block-policy return
set state-policy if-bound
set reassemble yes
set skip on lo0

# ......................................................................
# NORMALIZATION

match scrub ( min-ttl 255 random-id reassemble tcp )

# ......................................................................
# INCOMING FILTER RULES

pass  in     quick inet proto udp  $pubport = 68     $keepudp
pass  in     quick inet proto tcp  $pubport = 22     $keeptcp
pass  in     quick inet proto icmp icmp-type echoreq $keepudp
block in log quick all

# ......................................................................
# OUTGOING FILTER RULES

pass      out quick inet proto udp $pubport = 67  $keepudp
pass      out quick inet from (egress) to $inetok $keeptcp
block     out quick all
