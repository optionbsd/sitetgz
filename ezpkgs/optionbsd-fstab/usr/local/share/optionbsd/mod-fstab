#!/usr/bin/perl -w
# ======================================================================
# Copyright (C) 2007-2015, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------
use strict;
use warnings;
use autodie;
use Warr1024::OptionBSD::TweakLib;

# Make changes to the filesystem table.
modifile(
	'/etc/fstab',
	sub {
		my $gottmp;

		my @r = ();
		foreach $_ (@_) {

			# Keep track of whether or not we already have a /tmp
			# mount point.
			m#\s/tmp\s# and $gottmp++;

			# For all read/write FFS mounts, enable softdep and
			# noatime by default, as speed hacks.
			m#ffs\s+rw# and !m#noatime# and s#(\srw)#$1,noatime#;
			m#ffs\s+rw# and !m#softdep# and s#(\srw)#$1,softdep#;

			push @r, $_;
		}

		# If we don't have a /tmp mount, and we're able to determine the
		# amount of physical RAM in the system, create a mfs mount.
		# Limiting the size should reduce the risk of crashes or swapping
		# slowdown, and physical memory is a good size guideline.
		if(!$gottmp) {
			my $mem = `sysctl hw.usermem`;
			chomp($mem);
			$mem =~ m#=\s*(\d+)\s*$# and push @r,
			  "swap /tmp mfs rw,-s=" . int($1 / 640) . ",nodev,nosuid 0 0";
		}

		return \@r;
	});
