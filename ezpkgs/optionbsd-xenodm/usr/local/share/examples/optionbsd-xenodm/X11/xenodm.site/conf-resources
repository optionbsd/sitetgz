! ======================================================================
!              Aaron Suen's OpenBSD XDM Custom Configuration
! ======================================================================
! Copyright (C) 2007, Aaron Suen
!
! Permission to use, copy, modify, and/or distribute this software for
! any purpose with or without fee is hereby granted, provided that the
! above copyright notice and this permission notice appear in all
! copies.
!
! THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
! WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
! WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
! AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
! DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
! PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
! TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
! PERFORMANCE OF THIS SOFTWARE.
! ----------------------------------------------------------------------

! ALLOW ROOT LOGINS
! Comment out to disable
xlogin.Login.allowRootLogin:	true
xlogin.Login.allowNullPasswd:	true

xlogin*login.translations: #override \
	<Key>Left: move-backward-character()\n\
	<Key>Right: move-forward-character()\n\
	<Key>Home: move-to-begining()\n\
	<Key>End: move-to-end()\n\
	<Key>Return: finish-field()

xlogin*greeting: CLIENTHOST
xlogin*namePrompt: \040\040\040\040\040\040\040Login:
xlogin*fail: FAILED

#if WIDTH > 800
xlogin*greetFont: -adobe-helvetica-bold-o-normal--24-240-75-75-p-138-iso8859-1
xlogin*font: -adobe-helvetica-medium-r-normal--18-180-75-75-p-98-iso8859-1
xlogin*promptFont: -adobe-helvetica-bold-r-normal--18-180-75-75-p-103-iso8859-1
xlogin*failFont: -adobe-helvetica-bold-r-normal--18-180-75-75-p-103-iso8859-1
xlogin*greetFace:       DejaVu Sans Condensed-22:bold:italic:dpi=75
xlogin*face:            DejaVu Sans Condensed-16:dpi=75
xlogin*promptFace:      DejaVu Sans Condensed-16:bold:dpi=75
xlogin*failFace:        DejaVu Sans Condensed-16:bold:dpi=75
#else
xlogin*greetFont: -adobe-helvetica-bold-o-normal--17-120-100-100-p-92-iso8859-1
xlogin*font: -adobe-helvetica-medium-r-normal--12-120-75-75-p-67-iso8859-1
xlogin*promptFont: -adobe-helvetica-bold-r-normal--12-120-75-75-p-70-iso8859-1
xlogin*failFont: -adobe-helvetica-bold-o-normal--14-140-75-75-p-82-iso8859-1
xlogin*greetFace:       DejaVu Sans Condensed-18:bold:italic:dpi=75
xlogin*face:            DejaVu Sans Condensed-12:dpi=75
xlogin*promptFace:      DejaVu Sans Condensed-12:bold:dpi=75
xlogin*failFace:        DejaVu Sans Condensed-12:bold:dpi=75
#endif


#if !(defined(bpp1) || defined(bpp4) || defined(bpp8) || defined(bpp15))
# if PLANES < 4
#  ifndef bpp1
#   define bpp1
#  endif
# else
#  if PLANES > 4
#   if PLANES > 8
#    ifndef bpp15
#     define bpp15
#    endif
#   else
#    ifndef bpp8
#     define bpp8
#    endif bpp8
#   endif
#  else
#   ifndef bpp4
#    define bpp4
#   endif
#  endif
# endif
#endif  /* If manual override */

#ifndef bpp1
xlogin*borderWidth: 1
xlogin*frameWidth: 2
xlogin*innerFramesWidth: 0

xlogin.Login.sepWidth: 0

! top/left border
xlogin*hiColor: #667788

! bottom/right border
xlogin*shdColor: #667788

! 'Welcome to..' text color
xlogin*greetColor: #fef886
#if defined(bpp4) || defined(bpp8) || defined(bpp15)
! flood fill
xlogin*background: #798a99
xlogin*inpColor: #99aab9
#endif
xlogin*failColor: #aa0000

! 'Login:' and 'Password:'
*Foreground: #eeeeff

! border/shadow
*Background: #000000
#else
xlogin*borderWidth: 3
xlogin*frameWidth: 5
xlogin*innerFramesWidth: 1
xlogin*shdColor: white
xlogin*hiColor: white
xlogin*greetColor: white
xlogin*background: black
xlogin*failColor: white
xlogin*promptColor: white
*Foreground: white
*Background: black
#endif

xlogin*useShape: true
xlogin*logoPadding: 10

XConsole*background:	black
XConsole*foreground:	white
XConsole*borderWidth:	2
XConsole*borderColor:   grey
XConsole.text.geometry:	480x130
XConsole.verbose:	true
XConsole*iconic:	true
XConsole*font:		fixed

Chooser*geometry:		640x480
Chooser*allowShellResize:	false
Chooser*viewport.forceBars:	true

Chooser*label.font:	  -adobe-helvetica-bold-o-normal--24-*-p-*-iso8859-1
Chooser*label.label:	  XDMCP Host Menu from CLIENTHOST
Chooser*label.foreground: black
Chooser*list.font:	  lucidasanstypewriter-12
Chooser*Command.font:	  -adobe-helvetica-medium-r-normal--18-*-p-*-iso8859-1
