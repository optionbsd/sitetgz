#!/bin/sh
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin

# ======================================================================
#              Aaron Suen's OpenBSD XDM Custom Configuration
# ======================================================================
# Copyright (C) 2007, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------

########################################################################
## This program is run as as the user after logging in

# Debug output for xenodm.log.
echo '##### '${0##*/} $DISPLAY $USER

# Redirect errors to a file in /tmp.  It seems cleaner to keep them in
# volitile storage and not clutter up $HOME.  Error file should be tagged
# with current userid and display.
ERRFILE=/tmp/xsession-errors-$USER-`echo $DISPLAY | sed 's/[^A-Za-z0-9]/_/g'`
rm -f $ERRFILE
touch $ERRFILE
chmod 600 $ERRFILE
exec >$ERRFILE 2>&1

# If we have private ssh key(s), start ssh-agent and add the key(s).
# Also, register an exit trap to terminate the ssh-agent when the user's
# X session is over.
id1=$HOME/.ssh/identity
id2=$HOME/.ssh/id_dsa
id3=$HOME/.ssh/id_rsa
if [ -x /usr/bin/ssh-agent ] && [ -f $id1 -o -f $id2 -o -f $id3 ];
then
	eval `ssh-agent -s`
	ssh-add </dev/null
fi
trap 'if [ -n "$SSH_AGENT_PID" ]; then
	ssh-add -D < /dev/null
	eval `ssh-agent -s -k`
	fi
	exit 0 ' EXIT TERM INT HUP ALRM PIPE

# Capture width and height information from X resources and
# save into XWIDTH and XHEIGHT environment variables.
export XWIDTH=`echo : WIDTH | xrdb -n | cut -c 3-`
export XHEIGHT=`echo : HEIGHT | xrdb -n | cut -c 3-`

# Switch to a UTF-8 locale, as it's supported in X11,
# though it may not be in terminals.
export LC_CTYPE='en_US.UTF-8'
export LC_MESSAGES="$LC_CTYPE"

# Run user's .xsession program if it exists.  Otherwise,
# start fvwm as a fail-safe default.
STARTUP=$HOME/.xsession
if [ -s "$STARTUP" ]; then

	# Try, in order, running .xsession as a stand-alone
	# script, as a script in user's default shell, and
	# finally as a plain bourne shell script.
	if [ -x "$STARTUP" ]; then
		"$STARTUP"
	else
		if [ -n "$SHELL" ]; then
			"$SHELL" "$STARTUP"
		else
			/bin/sh "$STARTUP"
		fi
	fi

else

	# Load fvwm as a safe default.
	fvwm

fi
