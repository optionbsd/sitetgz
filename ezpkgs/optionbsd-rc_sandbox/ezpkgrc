name:		optionbsd-rc_sandbox
category:	sysutils
depends:	sysutils/optionbsd

summary:	OptionBSD Mod: rc.sandbox
maintname:	Aaron Suen
maintemail:	warr1024@gmail.com
www:		https://gitlab.com/Warr1024/openbsdsite/
fulldesc:	>
		Tools for creating "sandbox" mounts, which are ramdrive copies of local
		directory trees.  A sandbox mount will replace a directory tree at its
		original location, but will contain a snapshot copy of the contents of
		that tree.  Any changes made to that copy will not be persisted to disk,
		and will be lost on reboot.

		This is useful for:

		- Privacy, avoiding retaining potentially sensitive data by never
  		writing it to disk (outside of swap, which is aggressively encrypted
  		by default in OpenBSD).

		- Reducing wear on flash storage, by not making unnecessary writes to
  		frequently-changed but ephemeral data, such as dhcp leases, logs, or
  		other temp files.

		- Allowing all non-sandbox filesystems to be mounted read-only, avoiding
  		all write operations, and allowing the system to be rebooted quickly
  		without a proper shutdown, and no risk of filesystem corruption.

		Install a set of "sandbox" helper scripts on system startup:

		- /sbin/mksandbox, a shell script that automates creating a "sandbox"
  		using commands available relatively early in /etc/rc startup.

		- /etc/rc.sandbox, a shell script meant for easy customiation by an
  		administrator to add/remove sandboxes.

		- Add a hook to run rc.sandbox early in the /etc/rc process, after / is
  		mounted, but before mfs, /var, or /usr.  mksandbox will mount any of
  		these if necessary.

		Setup some default mounts as sandboxes.  Reasons why you might want to
		enable/disable each one may be listed in /etc/rc.sandbox:

		- /dev
		- /var/run
		- /var/tmp
		- /var/log

		Note that it is assumed that /tmp is already appropriately sanxboxed;
		this will have been done by the "fstab" mod, if installed.

		Sandbox mounts within NFS mounts are not recommended in rc.sandbox, as
		that process runs before network startup (necessary in case dhclient
		leases need to be written into a sandbox).  For sandboxes within non-
		local mounts, some customization may be needed.
