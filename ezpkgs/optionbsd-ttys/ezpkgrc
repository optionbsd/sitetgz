name:		optionbsd-ttys
category:	sysutils
depends:	sysutils/optionbsd

summary:	OptionBSD Mod: ttys
maintname:	Aaron Suen
maintemail:	warr1024@gmail.com
www:		https://gitlab.com/Warr1024/openbsdsite/
fulldesc:	>
	Tweak OpenBSD's standard built-in text consoles.

	The default configuration starts 5 or 6 different text terminals.  This
	is unnecessary, because: (1) users should probably be using tmux instead,
	which allows them to detach, allowing another user to log in, while also
	protecting that session from a simple vty change; and (2) many users will
	probably be using ssh, X11, or some other means of access.

	Disable all text terminals except for the first one.  The other vty's
	are still created in the kernel, but no process is attached to them by
	default.

	The default tty table lists all text consoles as using the vt220
	terminal type.  This is a safe, conservative option, but on i386/amd64,
	the pccon terminal type offers a better match with the terminals' actual
	capabilities.  Change the getty terminals to use pccon.

	The "console" terminal, which is used as an emergency recovery console
	when running single-user, is not touched.
