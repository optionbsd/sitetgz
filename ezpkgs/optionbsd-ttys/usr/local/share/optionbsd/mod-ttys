#!/usr/bin/perl -w
# ======================================================================
# Copyright (C) 2007-2015, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------
use strict;
use warnings;
use autodie;
use Warr1024::OptionBSD::TweakLib;

# Tweak the default set of system ttys.
modifile(
	"/etc/ttys",
	sub {
		my @r = ();
		foreach $_ (@_) {

			# Disable all ttys by default except ttyC0 and console.
			# Use tmux for multiple user access.
			m#^\s*ttyC0# or m#^\s*console# or s#(\s)on(\s)#$1off$2#;

			# By default, all ttys are defined as vt220, but on
			# i386/amd64, pccon is the best TERM setting to use, which
			# supports the true native capabilities of the terminal.
			# Leave console alone, since it's the "emergency" terminal
			# and shouldn't be messed with unnecessarily.
			m#^\s*ttyC# and s#vt220#pccon#;

			push @r, $_;
		}
		return \@r;
	});
