name:		optionbsd-ycron
category:	sysutils
depends:	sysutils/optionbsd sysutils/ycron

summary:	OptionBSD ycron
maintname:	Aaron Suen
maintemail:	warr1024@gmail.com
www:		https://gitlab.com/Warr1024/openbsdsite/
fulldesc:	>
	Installs the "ycron" alternative to cron, and changes the default root
	crontab to use it.

	ycron is a cron alternative that runs a simple sleep loop, running and
	managing a single command.  Multiple commands are managed by running
	multiple ycron instances.

	Some of the major features of ycron:

	- Jobs are scheduled based on intervals, not specific times of day.  If
	  a job schedule was "missed" it will be run at the next possible time.
	  This is especially helpful to ensure that daily/weekly/monthly maint
	  scripts are run, even on laptops that are not left powered overnight.

	- Next run time can be persisted in a file, so schedule information is
	  not lost across reboots.

	- All intervals can be randomized, allowing work to be staggered,
	  instead of a bunch of jobs all piling up at the top of the hour.

	For other features, see the ycron documentation.

	This mod installs ycron system-wide; it currently does not use a
	package, but ycron is just a single file at /usr/local/bin/ycron.

	This also replaces roots default crontab with one that uses ycron for
	all of the existing standard base jobs.  Persistence files for the
	daily/weekly/monthly maintenance scripts are stored in /var/db/ycron.
