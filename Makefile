# ======================================================================
# MAKEFILE -------------------------------------------------------------
# ======================================================================
# Copyright (C) 2007-2019, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------

VER=6.5
SNAP=6.5

PKG_REL=${SNAP}/packages/${ARCH}
OUTDIR=bin/${ARCH}
OUTFILE=${OUTDIR}/site`echo ${VER} | tr -cd 0-9`.tgz

all: ezpkg build

ezpkg:
	rm -rf ezpkgs/pkgs
	perl -w mkezpkg.pl

build:
	env ARCH=amd64 make tgz
	env ARCH=i386 make tgz

tgz: assemble
	mkdir -p ${OUTDIR}
	cd tmp/${ARCH} && tar czhfv ../../${OUTFILE}.new *
	mv -f ${OUTFILE}.new ${OUTFILE}

assemble:
	rm -rf tmp/${ARCH}
	mkdir -p tmp/${ARCH}/pkgs.site
	cp *.site tmp/${ARCH}
	cp pkglist tmp/${ARCH}/pkgs.site
	cp ezpkgs/pkgs/* tmp/${ARCH}/pkgs.site
	env PKG_REL=${PKG_REL} perl -w ./pkgfind.pl tmp/${ARCH}/pkgs.site
	chmod -R u+rwX,go+rX tmp/${ARCH}
	chmod a+x tmp/${ARCH}/*.site

clean:
	rm -rf ezpkgs/pkgs cache tmp bin
