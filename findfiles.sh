#!/bin/sh
# ======================================================================
# FIND FILES TO INCLUDE ------------------------------------------------
# This script compiles a list of files to include in the site archive,
# for piping to tar.
# ======================================================================
# Copyright (C) 2007-2014, Aaron Suen
#
# Permission to use, copy, modify, and/or distribute this software for
# any purpose with or without fee is hereby granted, provided that the
# above copyright notice and this permission notice appear in all
# copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
# DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
# PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# ----------------------------------------------------------------------
PATH=/bin:/usr/bin:/sbin:/usr/sbin
export PATH

# Include the install.site and upgrade.site files in the root.
ls -1 *.site

# Include everything in tweaks recursively.
find tweaks/ -type f -print -or -type l -print | sort -u
