#!/usr/bin/perl -w
use strict;
use warnings;
use autodie;

use Cwd "cwd";
use Digest::SHA ();
use File::Copy "cp";
use File::Find "find";
use File::Path "mkpath";
use File::Spec::Functions ":ALL";
use File::Temp ();
use POSIX qw(strftime);
use Text::Tabs "expand";
use Text::Wrap "wrap";
use YAML ();

use constant CONF => "ezpkgrc";

my $inroot = catdir(cwd(), "ezpkgs");
my $outroot = catdir(cwd(), "ezpkgs", "pkgs");

$Text::Wrap::columns = 80;

select STDERR;
$| = 1;

#@[mysys
sub mysys {
	use strict;

	my %allow = (0 => 1);
	ref $_[0] eq 'ARRAY'
	  and %allow = map { int($_) => 1 } @{shift()};

	my $r = system(@_);

	$r == -1 and die("@_: failed: $!");
	($r & 127)
	  and die(  "@_: died, signal "
		  . ($r & 127)
		  . ", coredump "
		  . (($r & 128) ? "present" : "absent"));
	$r = $r >> 8;
	$allow{$r}
	  or die(
		"@_: exited with value $r; expected " . join(", ", sort(keys %allow)));
	1;
}
#@]

sub slurp {
	open(my $fh, "<", $_[0]) or die($!);
	local $/;
	<$fh>;
}

sub spew {
	open(my $fh, ">", $_[0]) or die($!);
	print $fh $_[1];
}

sub plfile {
	my($t, $f, $outf) = @_;
	my $sha = Digest::SHA->new("sha256");
	open(my $fh, "<", $outf) or die($!);
	$sha->addfile($fh);
	close($fh);
	return( "\@$t $f",
		'@sha ' . $sha->b64digest() . "=",
		'@size ' . (-s $outf));
}

sub scanfiles(&) {
	find({wanted => $_[0], no_chdir => 1}, ".");
}

opendir(my $dh, $inroot) or die($!);
my @pkgs = grep { -s catfile($inroot, $_, CONF) } readdir($dh);
closedir($dh);

my %mkdirs = ();

for $_ (sort @pkgs) {
	chdir(catdir($inroot, $_)) or die($!);

	my %rc = %{YAML::Load(expand(slurp(CONF)))};
	map { $rc{$_} or die("missing required value \"$_\"") }
	  qw(name category summary);

	my $version = 0;
	scanfiles {
		my $t = strftime("%Y%m%d%H%M%S", localtime(-f $_ ? (stat($_))[9] : 0));
		$t > $version and $version = $t
	};

	my $tempdir = File::Temp->newdir();
	my $tmpd    = $tempdir->dirname();

	my @desc = join $/, map { chomp; wrap("", "", $_) }
	  grep { m#\S# } split(m#$#m, $rc{fulldesc} || '');
	$desc[0] = "$rc{summary}$/$desc[0]";
	$rc{maintname} && $rc{maintemail}
	  and push @desc, "Maintainer: $rc{maintname} <$rc{maintemail}>";
	$rc{www} and push @desc, "WWW: $rc{www}";
	spew(catfile($tmpd, "+DESC"), join("$/$/", @desc) . $/);

	my @deps = ();
	$rc{depends}
	  and @deps = map {
		m#:#    or s#([^/]+)$#$1:$1-*:$1-0#;
		m#:.*:# or s#:(.*)-\*#:$1-*:$1-0#;
		$_;
	  }
	  grep { $_ and m#\S+# }
	  map { split(m#\s+#, $_) }
	  ((ref $rc{depends} eq "ARRAY") ? @{$rc{depends}} : ($rc{depends}));

	my @plist = (
		"name $rc{name}-$version",
		"comment pkgpath=$rc{category}/$rc{name} cdrom=no ftp=no",
		"localbase /usr/local",
		"arch *",
		plfile("file", "+DESC", catfile($tmpd, "+DESC")));
	push @plist, map { "depend $_" } @deps;
	push @plist, "owner root", "group bin", "cwd /";
	@plist = map { m#^\@# ? $_ : "\@$_" } @plist;
	my @tar = qw(+CONTENTS +DESC);
	scanfiles {
		my @d  = grep { m#[^\.]# } splitdir($_);
		my $f  = catfile(@d);
		my $fr = pop @d;

		$f or return;
		$f eq CONF and return;
		-d $f and return;

		my $t        = "file";
		my %dirtypes = (
			"info" => "info",
			"rc.d" => "rcscript",
			"man"  => "man");
		map { $t = $dirtypes{$_} || $t } @d;

		mkpath(catdir($tmpd, @d));

		my $outf = catfile($tmpd, $f);
		sub {
			$t eq "man" or return;
			open(my $fh, "<", $f) or die($!);
			$_ = <$fh>;
			close($fh);
			m#pod2man:\s(.*)# or return;

			open($fh, "-|", "pod2man", catfile(@d, $1)) or die($!);
			my $m = do { local $/; <$fh> };
			close($fh);
			$? and die($?);

			spew($outf, $m);
			1;
		  }
		  ->()
		  or cp($f, $outf)
		  or die($!);

		push @plist, plfile($t, $f, $outf);

		my @rel = @d;
		shift @rel while (@rel and $rel[0] ne "examples");
		if(@rel) {
			shift @rel;
			shift @rel;
			my $mk = "etc/" . join("/", @rel);
			if(@rel and !$mkdirs{$mk}) {
				push @plist, "\@dir $mk";
				$mkdirs{$mk} = 1;
			}
			push @plist, "\@sample $mk/$fr";
		}

		push @tar, $f;
	};
	spew(catdir($tmpd, "+CONTENTS"), join("", map { "$_$/" } @plist));

	-e $outroot or mkpath($outroot);
	my $tgz = catfile($outroot, "$rc{name}-$version.tgz");
	my @cmd = qw(tar);
	`uname` =~ m#Linux#i and push @cmd, qw(-H ustar);
	mysys(@cmd, "-czf", $tgz, "-C", $tmpd, @tar);

	print "ezpkg: $tgz$/";
}
